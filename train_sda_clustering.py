'''
1)Run this code on 10.5.18.112 server
Login Using the following command
$ ssh -X 14EE10008@10.5.18.112

2)Change the working directory using the following command
$ cd termproject/sdautoencoder

3) Activate python2 environment using the following command
$ source activate py27

4)Now run the script using the following command
'''
from sklearn.cluster import KMeans
import tensorflow as tf
import pickle,csv,os,sys
import numpy as np
import pandas as pd


print("Loading Data Vectors")
with open('../data/spring16.p','rb') as pf:
    spring = pickle.load(pf)
springvectors = []

for di in range(len(spring['v1'])):
    x1 = np.reshape(np.array(spring['v1'][di]),(4800,1))
    x2 = np.reshape(np.array(spring['v2'][di]),(4800,1))
    x3 = np.reshape(np.array(spring['v3'][di]),(4800,1))
    x4 = np.reshape(np.array(spring['v4'][di]),(4800,1))
    x5 = np.reshape(np.array(spring['v5'][di]),(4800,1))
    X = np.concatenate([x1,x2,x3,x4,x5],axis = 0)
    X = np.transpose(X)
    springvectors.append(X)
print("Done Loading")


'''
Weight Initialization in this cell
'''

x = tf.placeholder(tf.float32,(None,24000))

W1 = tf.Variable(tf.random_uniform((24000,500),-0.1, 0.1))
B1 = tf.Variable(tf.zeros(500))
W2 = tf.Variable(tf.random_uniform((500,500),-0.1, 0.1))
B2 = tf.Variable(tf.zeros(500))
W3 = tf.Variable(tf.random_uniform((500,2000),-0.1, 0.1))
B3 = tf.Variable(tf.zeros(2000))
W4 = tf.Variable(tf.random_uniform((2000,10),-0.1, 0.1))
B4 = tf.Variable(tf.zeros(10))

W5 = tf.Variable(tf.random_uniform((10,2000),-0.1, 0.1))
B5 = tf.Variable(tf.zeros(2000))
W6 = tf.Variable(tf.random_uniform((2000,500),-0.1, 0.1))
B6 = tf.Variable(tf.zeros(500))
W7 = tf.Variable(tf.random_uniform((500,500),-0.1, 0.1))
B7 = tf.Variable(tf.zeros(500))
W8 = tf.Variable(tf.random_uniform((500,24000),-0.1, 0.1))
B8 = tf.Variable(tf.zeros(24000))


##encoder
Z1 = tf.maximum(tf.matmul(x,W1)+B1,0)
Z2 = tf.maximum(tf.matmul(Z1,W2)+B2,0)
Z3 = tf.maximum(tf.matmul(Z2,W3)+B3,0)
Z4 = tf.matmul(Z3,W4)+B4

##decoder
Z5 = tf.matmul(Z4,W5)+B5
Z6 = tf.maximum(tf.matmul(Z5,W6)+B6,0)
Z7 = tf.maximum(tf.matmul(Z6,W7)+B7,0)
Z8 = tf.matmul(Z7,W8)+B8

loss = tf.reduce_mean(tf.square(x-Z8))
opt = tf.train.AdamOptimizer(learning_rate=0.00001).minimize(loss)
saver = tf.train.Saver()

print("Started Training Stacked Denoising Autoencoder")
## Stacked Denoising Autoencoder Implementation

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    print("Stack-Layer 1")
    Z1 = tf.matmul(x,W1)+B1
    Z8 = tf.matmul(Z1,W8)+B8
    batch_size = 16
    di = 0
    noise = 0.1*np.random.rand(batch_size,24000)
    for l in range(0,4000,batch_size):
        if(l+batch_size>=len(springvectors)):
            break
        X = springvectors[l:l+batch_size]
        X = np.reshape(X,(batch_size,24000))
        X = X+noise
        di+=batch_size
        _, L = sess.run([opt, loss], feed_dict={x: X})
        if(di%100==0):
            print(di, L)
            
    print("Stack-Layer-2")
    
    Z1 = tf.matmul(x,W1)+B1
    Z2 = tf.matmul(Z1,W2)+B2
    Z7 = tf.matmul(Z2,W7)+B7
    Z8 = tf.matmul(Z1,W8)+B8
    di = 0
    for l in range(0,4000,batch_size):
        if(l+batch_size>=len(springvectors)):
            break
        X = springvectors[l:l+batch_size]
        X = np.reshape(X,(batch_size,24000))
        X = X+noise
        di+=batch_size
        _, L = sess.run([opt, loss], feed_dict={x: X})
        if(di%1000==0):
            print(di, L)
            
    print("Stack-Layer-3")
    Z1 = tf.matmul(x,W1)+B1
    Z2 = tf.matmul(Z1,W2)+B2
    Z3 = tf.matmul(Z2,W3)+B3
    Z6 = tf.matmul(Z3,W6)+B6
    Z7 = tf.matmul(Z2,W7)+B7
    Z8 = tf.matmul(Z1,W8)+B8
    di = 0
    for l in range(0,4000,batch_size):
        if(l+batch_size>=len(springvectors)):
            break
        X = springvectors[l:l+batch_size]
        X = np.reshape(X,(batch_size,24000))
        X = X+noise
        di+=batch_size
        _, L = sess.run([opt, loss], feed_dict={x: X})
        if(di%1000==0):
            print(di,L)
        
            
    print("Stack-Layer-4")
    Z1 = tf.matmul(x,W1)+B1
    Z2 = tf.matmul(Z1,W2)+B2
    Z3 = tf.matmul(Z2,W3)+B3
    Z4 = tf.matmul(Z3,W4)+B4
    Z5 = tf.matmul(Z4,W5)+B5
    Z6 = tf.matmul(Z3,W6)+B6
    Z7 = tf.matmul(Z2,W7)+B7
    Z8 = tf.matmul(Z1,W8)+B8
    di = 0
    for l in range(0,4000,batch_size):
        if(l+batch_size>=len(springvectors)):
            break
        X = springvectors[l:l+batch_size]
        X = np.reshape(X,(batch_size,24000))
        X = X+noise
        di+=batch_size
        _, L = sess.run([opt, loss], feed_dict={x: X})
        if(di%1000==0):
            print(di, L)
    save_path = saver.save(sess, "weights/model.ckpt")
    
'''
K-Means Clustering in this Cell
'''
with tf.Session() as sess:
    saver.restore(sess, "weights/model.ckpt")
    Z = []
    i = 0
    for X in springvectors:
        i+=1
        if(i==4000):
            break
        z = sess.run([Z4], feed_dict={x: X})
        Z.append(z)
        if(i%100==0):
            print(i)
    Z = np.array(Z)
    Z = np.reshape(Z,(len(Z),10))
    print(Z.shape)
    kmeans = KMeans(n_clusters=9, random_state=0).fit(Z)


with open('clustercenters.p','wb') as pfile:
    pickle.dump(kmeans.cluster_centers_,pfile)