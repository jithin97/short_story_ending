'''
1)Run this code on 10.5.18.112 server
Login Using the following command
$ ssh -X 14EE10008@10.5.18.112

2)Change the working directory using the following command
$ cd termproject/sdautoencoder

3) Activate python2 environment using the following command
$ source activate py27

4)Now run the script using the following command
'''


import tensorflow as tf
import pickle,csv,os,sys
import numpy as np
import pandas as pd

'''
Weight Initialization in this cell
'''

x = tf.placeholder(tf.float32,(None,24000))

W1 = tf.Variable(tf.random_uniform((24000,500),-0.1, 0.1))
B1 = tf.Variable(tf.zeros(500))
W2 = tf.Variable(tf.random_uniform((500,500),-0.1, 0.1))
B2 = tf.Variable(tf.zeros(500))
W3 = tf.Variable(tf.random_uniform((500,2000),-0.1, 0.1))
B3 = tf.Variable(tf.zeros(2000))
W4 = tf.Variable(tf.random_uniform((2000,10),-0.1, 0.1))
B4 = tf.Variable(tf.zeros(10))

W5 = tf.Variable(tf.random_uniform((10,2000),-0.1, 0.1))
B5 = tf.Variable(tf.zeros(2000))
W6 = tf.Variable(tf.random_uniform((2000,500),-0.1, 0.1))
B6 = tf.Variable(tf.zeros(500))
W7 = tf.Variable(tf.random_uniform((500,500),-0.1, 0.1))
B7 = tf.Variable(tf.zeros(500))
W8 = tf.Variable(tf.random_uniform((500,24000),-0.1, 0.1))
B8 = tf.Variable(tf.zeros(24000))


##encoder
Z1 = tf.maximum(tf.matmul(x,W1)+B1,0)
Z2 = tf.maximum(tf.matmul(Z1,W2)+B2,0)
Z3 = tf.maximum(tf.matmul(Z2,W3)+B3,0)
Z4 = tf.matmul(Z3,W4)+B4

##decoder
Z5 = tf.matmul(Z4,W5)+B5
Z6 = tf.maximum(tf.matmul(Z5,W6)+B6,0)
Z7 = tf.maximum(tf.matmul(Z6,W7)+B7,0)
Z8 = tf.matmul(Z7,W8)+B8

saver = tf.train.Saver()

with open('clustercenters2.p','rb') as pfile:
    clustercenters = pickle.load(pfile)
print("PreTrained-Cluster Centers are : ")
for cc in clustercenters:
    print(cc)
    
with open('../data/test.p','rb') as pfile:
    test = pickle.load(pfile)

testvectorsa = []
testvectorsb = []
for di in range(len(test['v1'])):
    x1 = np.reshape(np.array(test['v1'][di]),(4800,1))
    x2 = np.reshape(np.array(test['v2'][di]),(4800,1))
    x3 = np.reshape(np.array(test['v3'][di]),(4800,1))
    x4 = np.reshape(np.array(test['v4'][di]),(4800,1))
    x5a = np.reshape(np.array(test['v5a'][di]),(4800,1))
    x5b = np.reshape(np.array(test['v5b'][di]),(4800,1))
    Xa = np.concatenate([x1,x2,x3,x4,x5a],axis = 0)
    Xa = np.transpose(Xa)
    testvectorsa.append(Xa)
    Xb = np.concatenate([x1,x2,x3,x4,x5b],axis = 0)
    Xb = np.transpose(Xb)
    testvectorsb.append(Xb)
    
df = pd.read_csv('../data/test.csv')

with tf.Session() as sess:
    saver.restore(sess, "weights2/model.ckpt")
    print("Done restoring")
    testa = np.array(testvectorsa)
    testa = np.reshape(testa,(1871,24000))
    Za = sess.run([Z4], feed_dict={x: testa})
    testb = np.array(testvectorsb)
    testb = np.reshape(testb,(1871,24000))
    Zb = sess.run([Z4], feed_dict={x: testb})
    
Za = np.array(Za)
Za = np.reshape(Za,(1871,10))
Zb = np.array(Zb)
Zb = np.reshape(Zb,(1871,10))

scoresa = []
scoresb = []
gi = 0
for gi in range(len(Za)):
#     print(gi)

    z = Za[gi]
    Z = np.array(z)
    Z = np.reshape(Z,(10))
    sa9 = np.zeros(9)
    for j,cc in enumerate(clustercenters):
        cc = np.array(cc)
        sa9[j] = 1/(1+np.sum(np.square(Z-cc)))
    scoresa.append(max(sa9))

    z = Zb[gi]
    Z = np.array(z)
    Z = np.reshape(Z,(10))
    sb9 = np.zeros(9)
    for j,cc in enumerate(clustercenters):
        cc = np.array(cc)
        sb9[j] = 1/(1+np.sum(np.square(Z-cc)))
    scoresb.append(max(sb9))
    
y = 0
n = 0
s1 = 0
s2 = 0
finalresult = []

for i,row in df.iterrows():
    if(scoresa[i]>=scoresb[i]):
        select=1
        s1+=1
    else:
        select=2
        s2+=1
    finalresult.append(select)
    if(row['AnswerRightEnding']==select):
        y+=1
    else:
        n+=1
    
print("Accuracy is : {}".format(1.0*y/(y+n)))